class WebhookController < ApplicationController
  def create
    if params["push"]
      commit_message    = params["push"]["changes"][0]['new']['target']['message']
      commit_url        = params['push']['changes'][0]['new']['target']['links']['html']['href']
      issue_ids         = commit_message.scan(/(\#[\d]{4,})/).map{ |id| id[0].split("#")[1] }
      if issue_ids.any?
        notes           = "#{commit_message.split(issue_ids.last)[1].strip}\n#{commit_url}"
        ReportIssue.create_comment_on_redmine(issue_ids, notes)
      end
    elsif params["comment"]
      raw_comment       = params["comment"]["content"]["raw"]
      issue_ids_comment = raw_comment.scan(/(\#[\d]{4,})/).map{ |id| id[0].split("#")[1] }
      issue_ids_commit  = params["commit"]["message"].scan(/(\#[\d]{4,})/).map{ |id| id[0].split("#")[1] }
      parent_issue_id   = issue_ids_comment.any? ? issue_ids_comment.first : issue_ids_commit.first
      ReportIssue.create_issue_on_redmine(raw_comment, parent_issue_id ) if raw_comment
    end
    render :json => {"message" => "success"}
  end 
end
require 'net/http'
require 'csv'
class ReportIssue < ActiveRecord::Base

  def self.create_issue_on_redmine(subject, parent_issue_id)
    redmine_api       = CONFIG["redmine"]["redmine_api"]
    redmine_url       = URI.encode(CONFIG["redmine"]["url"])
    redmine_uri       = URI.parse(redmine_url)


    get_issue_url     = URI.encode(CONFIG["redmine"]["url"])+"/"+parent_issue_id+".json"
    get_issue_uri     = URI.parse(get_issue_url)
    get_issue_req     = Net::HTTP::Get.new(get_issue_url.to_s, nil) 
    get_issue_req['X-Redmine-API-Key']  = redmine_api
    res = Net::HTTP.start(get_issue_uri.hostname, get_issue_uri.port) do |http|
      http.request(get_issue_req)
    end
    res_data          = JSON.parse(res.body)

    project_id        = res_data["issue"]["project"]["id"]
    assigned_to_id    = res_data["issue"]["author"]["id"]

    tracker_name      = subject[/\[(.*?)]/,1]
    tracker_id        = CONFIG[:keywords][tracker_name]
    subject           = subject.split("[#{tracker_name}]").last.strip

    if tracker_id
      req = Net::HTTP::Post.new(redmine_uri.to_s, nil)
      req['X-Redmine-API-Key']  = redmine_api
      req['Content-Type']       = "application/x-www-form-urlencoded"
      req.set_form_data({"issue[project_id]" => project_id, "issue[tracker_id]" => tracker_id, 
                          "issue[subject]" => subject, "issue[parent_issue_id]" => parent_issue_id,
                          "issue[assigned_to_id]" => assigned_to_id})
      res = Net::HTTP.start(redmine_uri.hostname, redmine_uri.port) do |http|
        http.request(req)
      end
    end  
  end

  def self.create_comment_on_redmine(issue_ids, notes)
    redmine_api                 = CONFIG["redmine"]["redmine_api"]
    issue_ids.each do |issue_id|
      redmine_url               = URI.encode(CONFIG["redmine"]["url"])+"/"+issue_id
      redmine_uri               = URI.parse(redmine_url)
      req = Net::HTTP::Put.new(redmine_uri.to_s, nil)
      req['X-Redmine-API-Key']  = redmine_api
      req['Content-Type']       = "application/x-www-form-urlencoded"
      req.set_form_data({"issue[notes]" => notes })
      res = Net::HTTP.start(redmine_uri.hostname, redmine_uri.port) do |http|
        http.request(req)
      end
    end
  end
end

